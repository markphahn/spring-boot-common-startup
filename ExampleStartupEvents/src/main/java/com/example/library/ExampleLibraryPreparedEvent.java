package com.example.library;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class ExampleLibraryPreparedEvent implements ApplicationListener<ApplicationPreparedEvent> {

    private static final Logger log = LoggerFactory.getLogger(ExampleLibraryPreparedEvent.class);

    @Override
    public void onApplicationEvent(ApplicationPreparedEvent event) {
        log.info(" **** **** Example *library* startup ApplicationListener#ApplicationPreparedEvent()");
    }

}