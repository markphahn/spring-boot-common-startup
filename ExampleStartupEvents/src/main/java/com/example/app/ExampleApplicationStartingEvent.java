package com.example.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(5)
public class ExampleApplicationStartingEvent implements ApplicationListener<ApplicationStartingEvent> {

    private static final Logger log = LoggerFactory.getLogger(ExampleApplicationStartingEvent.class);

    @Override
    public void onApplicationEvent(ApplicationStartingEvent event) {
        log.info(" **** **** Example application startup ApplicationListener#ApplicationStartingEvent()");
    }

}