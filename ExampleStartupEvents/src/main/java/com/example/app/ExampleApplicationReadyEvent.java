package com.example.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(4)
public class ExampleApplicationReadyEvent implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger log = LoggerFactory.getLogger(ExampleApplicationReadyEvent.class);

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        log.info(" **** **** Example application startup ApplicationListener#onApplicationEvent()");
    }

}