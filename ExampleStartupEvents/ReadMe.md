# Overview
This example attempts to have a library (or what will be a library)
tap into Spring Boot Startup events.

A basic Spring Boot app is in the package `com.example.app` while the
library is the the package `com.example.library`. The classes in
`com.example.library` attempt to register listeners for application
creation events. 

The reason for tapping into Spring Boot Startup events is to provide
common startup for applications across a corporate or organization
application ecosystem.

