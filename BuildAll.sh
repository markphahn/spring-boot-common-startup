#!
(cd ExampleStartup ; pwd ; gradle clean assemble )
(cd ExampleStartupEvents ; pwd ; gradle clean assemble )
(cd ExampleStartupFunction ; pwd ; gradle clean assemble )
(cd ExampleStartupFluent ; pwd ; gradle clean assemble )

ls -l */build/libs/*
