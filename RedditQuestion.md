# Reddit Post Title

How to tap into Spring Boot startup from a library in a Spring Boot way?

# Question

I am building a library to be used across multiple apps for a large
organization. I will use Example.com to illustrate. Ideaily my Maven
packaged library will allow a simple annotation to perform common
startup across multiple Spring Boot services / microservices /
applicaitons.

I would like to use an annotation in the applications main
class like this:

```
package com.example.app;

import . . . 

@ExampleCorporateStartup
@EnableAutoConfiguration
@SpringBootApplication
public class ExampleApp {
    public static void main(String[] args) {
      . . . 
```

However, I do not understand how to get tapped into the Spring
Boot Runtime to perform my common startup.

I have built a `GitLab` repository with my sample code for more
detailed examination at:
https://gitlab.com/markphahn/spring-boot-common-startup.

# Approaches tried

I have tried several approachs for this, as described below:

## Spring Boot `ExampleApplicationReadyEvent`

I tried to register a listener for `ExampleApplicationReadyEvent` but
it does not get called:

```
package com.example.library;

import . . . 

@Component
public class ExampleLibraryEnvironmentPreparedEvent
        implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {
    private static final Logger log = LoggerFactory.getLogger(ExampleLibraryEnvironmentPreparedEvent.class);
    @Override
    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
        log.info(" **** **** Example *library* startup ApplicationListener#ApplicationEnvironmentPreparedEvent()");
    }

}
```

I do not know if this is because I do not have the right annotations,
or what. 

Then, even if my listener gets called, how would I use my annotation
to trigger what I want. I think I would need to use reflection to scan
for classes which have my annotation, but there does not seem to be a
way to write `for (Class c : Object.getClass().getSubTypes())`. I am
thinking about this in the wrong way (probably).

In the 'GitLab' repo see the code in `ExampleStartupEvents` directory.

Reference:
https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-application-events-and-listeners


## Write a function called before `SpringBootApplication.run()`

This works but it seems non-Spring-y:

```
package com.example.app;

import . . . 

@EnableAutoConfiguration
@SpringBootApplication
public class ExampleApp {

    static Logger log = LoggerFactory.getLogger(ExampleApp.class);

    public static void main(String[] args) {
        ExampleCorporateStartup.startup(); // **** **** common startup

        SpringApplication.run(ExampleApp.class, args);
    }
}
```

This has a disatvantage that it is called before Spring has
initialized the logging layer and the log messages appear for the
Spring banner. This is not a functional problem, but it looks both
un-Spring-y and unprofessional. 

Here is a variation on the above:

```
    public static void main(String[] args) {
        SpringApplication myApp = new SpringApplication(ExampleApp.class);
        ExampleCorporateStartup.startup(myApp); // **** **** common startup
        myApp.run();
    }
```

This is a variation on the funciton approach, but it takes a lot of
control out of the developer's hands, and it not the approach I want
to take:

```
    public static void main(String[] args) {
        ExampleCorporateStartup.run(ExampleApp.class, args);
    }
```

In the 'GitLab' repo see the code in `ExampleStartupFunction` directory.

Reference:
https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-customizing-spring-application

There is also a problem that it is un-Fluent startup.

## Write a Fluent-style function as well

This might look like this:
```
package com.example.app;

import . . . 

@EnableAutoConfiguration
@SpringBootApplication
public class ExampleApp {

    static Logger log = LoggerFactory.getLogger(ExampleApp.class);

    public static void main(String[] args) {
           new ExampleCorporateStartupSpringApplicationBuilder()
            .sources(ExampleApp.class)
            .child(ExampleApp.class)
            .run(args);
```

But now I have to maintain two functions which do the same startup
work. I can have them call the same core functionality, but they
differ in how that functionality is expressed: fluent-ly or
imperative-ly. 

In the 'GitLab' repo see the code in `ExampleStartupFluent` directory.

Reference:
https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-fluent-builder-api

# The question(s):

What is the right approach for a corporate library package which
provides the right amount of common assistance and but does not limit
the developer too much? (I do not want to replace Sping Boot classes
with my own, I want to augment them in the correct way.)

Has this already been done a million times and I just have not found
the right examples? If so, where are those examples?

Given an approach, how does one pull this off, as in is the example in
the public domain I can copy?

# The rabbit holes:

Why are my events not registered by creating an `@Component` which
implements `ApplicationListener`?

How would I use an annotation at runtime to find my main class and
perform the common startup that I desire?

