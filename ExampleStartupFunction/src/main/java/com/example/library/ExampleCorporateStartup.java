package com.example.library;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;

import com.example.app.ExampleApp;

public class ExampleCorporateStartup {
    static Logger log = LoggerFactory.getLogger(ExampleApp.class);

    public static void startup() {
        log.info(" **** **** performing commmon corporate startup");

    }

    public static void startup(SpringApplication myApp) {
        log.info(" **** **** performing commmon corporate startup on app {}", myApp);
        myApp.addListeners(new ExampleLibraryApplicationEnvironmentPreparedEvent());
    }

}
