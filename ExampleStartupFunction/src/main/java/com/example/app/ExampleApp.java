package com.example.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.library.ExampleCorporateStartup;

@EnableAutoConfiguration
@SpringBootApplication
public class ExampleApp {

    static Logger log = LoggerFactory.getLogger(ExampleApp.class);

    public static void alternateMain(String[] args) {
        log.info("start - {}", System.getProperty("user.name"));
        log.info("{}", ExampleApp.class.toString());

        ExampleCorporateStartup.startup();

        SpringApplication.run(ExampleApp.class, args);
    }

    public static void main(String[] args) {
        log.info("start - {}", System.getProperty("user.name"));
        log.info("{}", ExampleApp.class.toString());

        SpringApplication myApp = new SpringApplication(ExampleApp.class);

        ExampleCorporateStartup.startup(myApp);

        myApp.run();
    }
}