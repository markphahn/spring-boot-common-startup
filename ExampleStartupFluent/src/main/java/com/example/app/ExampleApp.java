package com.example.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.library.ExampleCorporateStartupSpringApplicationBuilder;

@EnableAutoConfiguration
@SpringBootApplication
public class ExampleApp {

    static Logger log = LoggerFactory.getLogger(ExampleApp.class);

    public static void main(String[] args) {
        // @formatter:off
        new ExampleCorporateStartupSpringApplicationBuilder()
            .sources(ExampleApp.class)
            .child(ExampleApp.class)
            .run(args);
        // @formatter:on

    }
}