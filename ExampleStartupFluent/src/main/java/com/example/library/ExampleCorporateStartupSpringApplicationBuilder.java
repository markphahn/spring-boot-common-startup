package com.example.library;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.builder.SpringApplicationBuilder;

import com.example.app.ExampleApp;

public class ExampleCorporateStartupSpringApplicationBuilder extends SpringApplicationBuilder {
    static Logger log = LoggerFactory.getLogger(ExampleApp.class);

    public ExampleCorporateStartupSpringApplicationBuilder() {
        logStartupInfo(true);
        log.info(" **** **** fluent corproate startup");

        listeners(new ExampleLibraryEnvironmentPreparedEvent());
    }

}
