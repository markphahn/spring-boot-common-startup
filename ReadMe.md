# Overview

How to tap into Spring Boot startup from a library in a Spring Boot way?

I am building a library to be across multiple apps for a large
organization. I will use Example.com to illustrate. Ideaily my Maven
packaged library will allow a simple annotation to perform common
startup across multiple Spring Boot services / microservices /
applicaitons. 

I would like to use an annotation in the applications main
class like this:

```
@ExampleCorporateStartup
@EnableAutoConfiguration
@SpringBootApplication
public class ExampleApp {
    public static void main(String[] args) {
      . . . 
```

However, I do not understand how to get tapped into the Spring
Boot Runtime to perform my common startup.

See the Spring Tool Suite project `ExampleStartup`, which is a
non-fuctional skeleton of what I want to accomplish.

# Reddit

This is a question I have posted to `reddit` on `r/SpringBoot`. See `[RedditQuestion.md]`.

# Tapping into startup

In the example in `ExampleStartupEvents` I tried setting up listeners
for application startup events like
`ApplicationEnvironmentPreparedEvent` or `ExampleLibraryPreparedEvent`
but these do not seem to be called. The events in a separate package
(`com.example.library`) never seem to be called, while for the event
listeners in `com.example.app` only the `ExampleApplicationReadyEvent`
seems to be called.

I am doing something wrong here, but I canont spot it.


